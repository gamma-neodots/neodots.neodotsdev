# .dev: Hooks and tools for easing development on this submodule-based project

This submodule will set the hooks directory for all initialized modules.
The `post-commit` hook will automatically commit in the root repo when a submodule creates a commit.

